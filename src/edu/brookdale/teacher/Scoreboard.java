package edu.brookdale.teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/* FOR TEACHER USE ONLY!!!
 * This is the scoreboard program. Execute this periodically to show the current score
 */
public class Scoreboard {

	public static void main(String[] args) throws Exception {

		Connection connection = null;
		Statement stmt = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");  
			connection = DriverManager.getConnection("jdbc:mysql://" + Config.teacherComputerIP + ":" 
					+ Config.dbPort + "/" + Config.dbName + Config.dbOptions , Config.dbRootUser , Config.dbRootPass);
			connection.setAutoCommit(true);
			stmt = connection.createStatement();

			//query the records
			ResultSet rs = stmt.executeQuery("select name,count(*) from thehill group  by name order by count(*) desc");
			System.out.println("   King of the Hill\n");
			System.out.println(String.format("%-12s%10s" , "Name", "Count"));
			System.out.println("----------------------");
			while (rs.next()) {
				System.out.println(String.format("%-12s%10s" , rs.getString(1) , rs.getString(2)));
			}			

			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}
}


