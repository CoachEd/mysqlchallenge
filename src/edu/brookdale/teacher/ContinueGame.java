package edu.brookdale.teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/* FOR TEACHER USE ONLY!!!
 * This continues the game by giving back student user permissions. Call this after running StopGame.
 */
public class ContinueGame {

	public static void main(String[] args) throws Exception {

		Connection connection = null;
		Statement stmt = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");  
			connection = DriverManager.getConnection("jdbc:mysql://" + Config.teacherComputerIP + ":" 
					+ Config.dbPort + "/" + Config.dbName + Config.dbOptions , Config.dbRootUser , Config.dbRootPass);
			connection.setAutoCommit(true);
			stmt = connection.createStatement();

			//Give back Student permissions to INSERT and SELECT
			stmt.execute("GRANT INSERT ON " + Config.dbName + ".* TO 'student'@'%'");
			stmt.execute("GRANT SELECT ON " + Config.dbName + ".* TO 'student'@'%'");
			System.out.println("Game resumed.");			

			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}
}


