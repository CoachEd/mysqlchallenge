package edu.brookdale.teacher;

/*
 * FOR TEACHER ONLY!!! DO NOT GIVE TO STUDENTS.
 * TODO: Update the teacherComputerIP variable with the teacher computer's IP address
 */
public class Config {

	public static final String teacherComputerIP = "172.31.148.12"; // TODO: Update with teacher computer's current IP address
			
	public static final String dbRootUser = "root"; //TODO: Put the MySQL root username here
	public static final String dbRootPass = "root"; //TODO: Put the MySQL root password here
	
	public static final String dbPort = "3306";
	public static final String dbName = "mydb";
	public static final String dbOptions ="?autoReconnect=true&useSSL=false";
	
}
