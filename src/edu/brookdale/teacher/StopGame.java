package edu.brookdale.teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/* FOR TEACHER USE ONLY!!!
 * This stops the game by revoking student user permissions.
 */
public class StopGame {

	public static void main(String[] args) throws Exception {

		Connection connection = null;
		Statement stmt = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");  
			connection = DriverManager.getConnection("jdbc:mysql://" + Config.teacherComputerIP + ":" 
					+ Config.dbPort + "/" + Config.dbName + Config.dbOptions , Config.dbRootUser , Config.dbRootPass);
			connection.setAutoCommit(true);
			stmt = connection.createStatement();

			//Revoke student permissions
			stmt.execute("REVOKE ALL PRIVILEGES on " + Config.dbName + ".* FROM 'student'@'%';");
			System.out.println("Game ended.");			

			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}

	}
}


