package edu.brookdale.teacher;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/* FOR TEACHER USE ONLY!!! 
 * This is the setup program. The teacher must execute this program once, before the start of each new game.
 */
public class Setup {

	public static void main(String[] args) throws Exception {

		Connection connection = null;
		Statement stmt = null;

		try {
			Class.forName("com.mysql.jdbc.Driver");  
			connection = DriverManager.getConnection("jdbc:mysql://" + Config.teacherComputerIP + ":" 
					+ Config.dbPort + Config.dbOptions , Config.dbRootUser , Config.dbRootPass);
			connection.setAutoCommit(true);
			stmt = connection.createStatement();

			stmt.execute("drop database if exists " + Config.dbName);
			stmt.execute("create database " + Config.dbName);
			stmt.execute("drop user if exists student");
			stmt.execute("create user 'student'@'%' identified by 'password'");
			stmt.execute("USE " + Config.dbName);
			stmt.execute("CREATE TABLE `thehill` (`id` varchar(10) NOT NULL, `name` varchar(20) NOT NULL, primary key (`id`))");
			stmt.execute("grant insert on " + Config.dbName + ".* to 'student'@'%'");
			stmt.execute("grant select on " + Config.dbName + ".* to 'student'@'%'");

			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("Setup complete.");
	}
}


