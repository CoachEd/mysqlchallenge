package edu.brookdale.student;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * This is the student (game player) program. Students should try this first, to establish connectivity.
 * Then they may explore ways to insert records better, faster.
 * TODO: The teacher must provide the current IP address of the teacher computer.
 */
public class Example {

	public static void main(String[] args) throws Exception {

		Connection connection = null;
		Statement stmt = null;

		String teacherComputerIP = "172.31.148.12"; //TODO: Update with teacher computer's current IP address

		String dbUser = "student";
		String dbPass = "password";
		String dbPort = "3306";
		String dbName = "mydb";
		String dbOptions ="?autoReconnect=true&useSSL=false";

		try {
			Class.forName("com.mysql.jdbc.Driver");  
			connection = DriverManager.getConnection("jdbc:mysql://" + teacherComputerIP + ":" + dbPort + "/" + dbName + dbOptions,dbUser,dbPass);
			connection.setAutoCommit(true);
			stmt = connection.createStatement();

			//insert records with a unique id and your name
			stmt.execute("INSERT INTO thehill(id, name) VALUES('key1','Edwin')");
			stmt.execute("INSERT INTO thehill(id, name) VALUES('key2','Edwin')");
			stmt.execute("INSERT INTO thehill(id, name) VALUES('key3','Edwin')");

			//query the records
			ResultSet rs = stmt.executeQuery("SELECT * FROM thehill");
			System.out.println("Your records in the table:");
			while (rs.next()) {
				System.out.println(rs.getString("id") + " , " + rs.getString("name"));
			}				

			stmt.close();
		} catch (SQLException e) {
			System.out.println("ERROR: " + e.getMessage());
		}
	}
}


