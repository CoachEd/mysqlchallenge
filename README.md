# King of the Hill

#### A MySQL Programming Challenge

This is a fun programming challenge for first or second year Java programming students. Students compete with each other to insert the most records into a MySQL database. The student with the most records at the end of a set time is the King of the Hill (winner).

## Gameplay:

* Students only have permissions to INSERT or SELECT (no UPDATE or DELETE)
* Set a time limit (e.g., 15 or 30 minutes)
* Have prizes for 1st, 2nd, and 3rd place, as well as consolation prizes.
* Start the game: teacher executes the *Setup* Java program
* Periodically monitor the game: teacher periodically runs the *Scoreboard* Java program to show "The Hill"
    * Frequently showing the hill is part of the **fun!**
* End the game: teacher runs the *StopGame* Java program to revoke student permissions
* Continue game (after running StopGame): teacher runs the *ContinueGame* Java program to restore student permissions

## Teacher Setup

* Install MySQL on the teacher machine
    * Start -> Programs -> MySQL Installer
    * Remember the root username and password (you will need this later)
    * Start MySQL server (if not started already)  
* Get the IP address of the teacher machine (you will need this later)
    * From a Windows cmd prompt, issue *ipconfig* to get the IP address
* Clone this repository and import it as an existing project in Eclipse
* From the mysqlchallenge Eclipse project
    * Edit *Config.java*: modify the *teacherComputerIP*, *dbRootUser*, and *dbRootPass* variables (see TODO comments)  
    * Run the Setup program to set up the database, table, and student username
    * Run Setup at the start of each new game

## Student Setup

* Students must be on a classroom computer (no personal computers, no WiFi)
* Create a new Java project in Eclipse
* Students download the *Example.java* file and add it to their Eclipse projects
* Students download the *lib/mysql-connector-java-5.1.39-bin.jar* file and add it to their Eclipse projects
    * Right-click *project name* -> Properties -> Java Build Path -> Libraries -> Add External JARs...
* Students run the *Example.java* file to understand how it works
    * Students may modify this program to make it better, faster, more efficient


Enjoy!

Ed Torres